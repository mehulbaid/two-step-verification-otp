const express = require('express');
var http = require("https");

const app = express();

const authkey = process.env.msg91Authkey';

var val = Math.floor(1000 + Math.random() * 9000)
var sentotp = {};

app.use(express.json());

app.post('/getOtp', (requ, res, next) => {

    sentotp.val = val;

    const message = `Your OTP for BuyMyJewel is ${sentotp.val}`;

    console.log('Mobile: ' + requ.body.mobile);

    var options = {
        "method": "POST",
        "hostname": "api.msg91.com",
        "port": null,
        "path": "/api/v2/sendsms",
        "headers": {
            "authkey": authkey,
            "content-type": "application/json"
        }
    };

    var req = http.request(options, function (resp) {
        var chunks = [];

        resp.on("data", function (chunk) {
            chunks.push(chunk);
        });

        resp.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
            res.status(200).send(body.toString());
        });
    });

    req.write(JSON.stringify({
        sender: 'BYMYJL',
        route: '4',
        country: '91',
        sms:
            [{ message: message, to: [requ.body.mobile] }]
    }));

    req.end()

});

/** ('/verifyOtp') [POST] Request For Verifying OTP */
app.post('/verifyOtp', (req, res, next) => {

    if (req.body.val == sentotp.val) {
        res.status(200).send({ verfied: true });
    } else {
        res.status(200).send({ verfied: false });
    }
})

app.listen(3000, () => {
    console.log('\nListening on port 3000\n');
})