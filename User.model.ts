const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userBasicSchema = new Schema({
    first_name: String,
    last_name: String,
    username: String,
    email: { type: String, unique: true },
    mobileNumber: { type: String, unique: true },
    password: String, 
    accountType: String,
    isActive: { type: Number, default: 1},
    created_date: { type: Date, default: Date.now() },
    modified_date: { type: Date, default: Date.now() },
    delete_flag: { type: Number, enum: [0, 1], default: 0 }
}, { versionKey: false });

// function hashPassword(this: any, next: (err?: NativeError) => void) {
//     const user = this, SALT_FACTOR = 5;

//     if (!user.isModified('password')) {
//         return next();
//     }

//     bcrypt.hash(user.password, SALT_FACTOR)
//         .then((hash) => {
//             user.password = hash;
//         })
//         .then(() => next())
//         .catch(next);
// }

// userBasicSchema.pre('save', hashPassword);

const UserDetailModels = mongoose.model('users', userBasicSchema);

module.exports = UserDetailModels;